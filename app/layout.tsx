import './globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import { GlobalProvider } from './global-context'
import { Suspense } from 'react'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Task Tracker',
  description: 'mojtaba morsali task tracker demo app',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <main className="flex justify-center py-5 px-3">

        <GlobalProvider>
          <Suspense fallback={<p>Loading...</p>}>
            {children}
          </Suspense>
        </GlobalProvider>
        </main>

      </body>
    </html>
  )
}
