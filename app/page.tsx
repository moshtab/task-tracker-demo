'use client'

import { useState } from "react";
import { Button, Card, CardContent, Checkbox, Chip, IconButton, TextField, Typography } from '@mui/material';
import { Task, useGlobalContext } from "./global-context";
import Link from "next/link";
import DeleteIcon from '@mui/icons-material/Delete';

export default function Page() {
  const { setTask, tasks } = useGlobalContext();
  const [inputData, setInputData] = useState<string>('');

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputData(e.target.value);
  };

  const handleAddData = () => {
    if(!inputData || inputData.length < 1) {
      alert('enter task')
      return;
    }
    const hasSameTitle = tasks.some(a => a.title == inputData);
    if(!hasSameTitle) {
      setTask([...tasks, { title: inputData, isDone: false }]);
    } else {
      alert('this title already exist')
    }
  };

  const handleDeleteData = (task: Task) => {
    const newTasks = tasks.filter(a => a.title != task.title);
    setTask([...newTasks]);
  };

  const handleChangeisDone = (task: Task) => {
    const newTasks = tasks.filter(a => a.title != task.title);
    setTask([...newTasks, task]);
  };

  return (
    <div className="flex flex-col items-center justify-center w-2/4">
      <TextField label="Enter new task" variant="outlined"
        className="my-3 w-full"
        value={inputData}
        required
        onChange={handleInputChange} />

      <Button className="w-full mb-5" variant="outlined" color="primary" onClick={handleAddData}>
        Add
      </Button>

      <h1>Tasks</h1>

      <div className="w-full">
        {tasks.map(a => {
          return <Card key={a.title} className="mt-3 relative w-full min-w-fit">
            <CardContent>
              <IconButton className="absolute"
                style={{ top: 5, right: 5 }}
                aria-label="delete"
                color="error"
                onClick={(e) => {
                  e.preventDefault();
                  handleDeleteData(a);
                }}>
                <DeleteIcon />
              </IconButton>
              <Link href={`task/${a.title}`}>
                <Typography gutterBottom>
                  {a.title}
                </Typography>
              </Link>
              <div className="flex items-center">
                <label>Done</label>
                <Checkbox checked={a.isDone} onChange={(e) => {
                  console.log(e.target.checked)
                  e.preventDefault();
                  handleChangeisDone({ ...a, isDone: e.target.checked })
                }} />
              </div>
            </CardContent>
          </Card>
        })}
      </div>
    </div>
  )
}