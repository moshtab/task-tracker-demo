'use client'

import { useEffect, useState } from "react";
import { Task, useGlobalContext } from "../../global-context";
import { TextField, Button, Checkbox } from "@mui/material";
import { useRouter } from 'next/navigation'

export default function Page({ params }: { params: { title: string } }) {
    const { setTask, tasks } = useGlobalContext();
    const [inputData, setInputData] = useState('');
    const [isDone, setIsDone] = useState<boolean>(false);
    const router = useRouter();

    // Define the effect to run when the component mounts
    useEffect(() => {
        debugger
        const task = tasks.find(a => a.title == decodeURIComponent(params.title));
        if (task) {
            setInputData(task.title);
            setIsDone(task.isDone);
        } else {
            router.push('/', { scroll: false })
        }
    }, [tasks]);

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInputData(e.target.value);
    };

    const handleUpdateData = () => {
        const newTasks = tasks.filter(a => a.title != decodeURIComponent(params.title));
        setTask([...newTasks, { title: inputData, isDone }]);
        router.push(`/task/${inputData}`, { scroll: false })
    };

    const handleDeleteData = () => {
        const newTasks = tasks.filter(a => a.title != decodeURIComponent(params.title));
        setTask([...newTasks]);
        router.push('/', { scroll: false })
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setIsDone(event.target.checked);
    };

    return (
        <div className="flex flex-col items-center justify-center w-2/4">
            <TextField label="Task" variant="outlined"
                className="my-3 w-full"
                value={inputData}
                onChange={handleInputChange}
                placeholder="Enter new task" />

            <div className="flex items-center">
                <label>Done</label>
                <Checkbox
                    checked={isDone}
                    onChange={handleChange}
                />
            </div>
            <div className="flex justify-between w-full">
                <Button variant="outlined" color="error" onClick={handleDeleteData}>
                    Delete
                </Button>
                <Button variant="outlined" color="primary" onClick={handleUpdateData}>
                    Update
                </Button>
            </div>
        </div>
    )
}