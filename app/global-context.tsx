'use client'

import { createContext, useContext, useEffect, useState } from 'react';

export interface Task {
    title: string,
    isDone: boolean
}

interface GlobalState {
    tasks: Task[];
    setTask: any
}

const GlobalContext = createContext<GlobalState | undefined>(undefined);

//custom hook to access the context
export const useGlobalContext = () => {
    const context = useContext(GlobalContext);
    if (!context) {
        throw new Error('useGlobalContext must be used within a GlobalProvider');
    }
    return context;
};

export const GlobalProvider = ({children}: any) => {
    const [state, setState] = useState<GlobalState>({
        tasks: [],
        setTask: null 
    });

    const setTask = (data: Task[]) => {
        setState((prevState) => ({ ...prevState, tasks: data }));
    };

    useEffect(() => {
        const storedState = localStorage.getItem('globalState');
        if (storedState) {
            setState(JSON.parse(storedState));
        }
    }, []);

    useEffect(() => {
        localStorage.setItem('globalState', JSON.stringify(state));
    }, [state]);

    return (
        <GlobalContext.Provider value={{ ...state, setTask }}>
            {children}
        </GlobalContext.Provider>
    );
};